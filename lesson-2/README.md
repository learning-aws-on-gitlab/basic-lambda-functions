# Basic Lambda Functions - Lesson 2

Now we know how a Lambda function looks like. We saw that how we can make it
work with API Gateway.

Let's continue with adding multiple lambda functions to our API Gateway and use
exact methods for all of them. Do some verification with the event params. Then
try to interact with a DynamoDB table.

# Basic Lambda Functions

At the end of this series of meetings, I want to give you a meaningful
knowledge about AWS, Lambda, API Gateway, DynamoDB and GitLab CI/CD.

This can help you to get a job easier, and you will have a basic knowledge
of things that are used by most of the companies.

## [Lesson 1](lesson-1/README.md)

> Create and test a basic AWS Lambda with API Gateway and Postman

## [Lesson 2](lesson-2/README.md)

> TBD

import json
import math

def lambda_handler(event, context):

    body = json.loads(event["body"])
    
    a = body["a"]
    b = body["b"]
    
    pythagorean = math.sqrt(a*a + b*b)

    return {
        'statusCode': 200,
        'body': pythagorean
    }

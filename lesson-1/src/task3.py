import json
import math

def lambda_handler(event, context):

    httpMethod = event["httpMethod"]    
    body = json.loads(event["body"])
    text = body["text"]

    if httpMethod == "GET":
        result = text
    elif httpMethod == "POST":
        result = text + " created"
    elif httpMethod == "DELETE":
        result = text[0:(len(text)//2)]
    elif httpMethod == "PUT":
        result = text + text
    else:
        result = "Error"

    return {
        'statusCode': 200,
        'body': result
    }

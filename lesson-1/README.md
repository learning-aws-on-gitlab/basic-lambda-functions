# Basic Lambda Functions - Lesson 1

At the first meeting, we checked together what is AWS, How it looks and what main tools it has.

We also made our first Lambda function in python and we were testing it using Postman through API Gateway.

## Homework

Now it's time to do some homework.

### 1. Hello World

Create a Hello World lambda function and assign it to an API Gateway

### 2. Pythagoras

Create a lambda function with an API Gateway, that I can call with a JSON body like: `{ "a": 5, "b": 81 }`
and the returned value should be a number, which returns the the value of The Pythagorean theorem.

### 3. Reverse String

Create a lambda function which is can process a string and based on the HTTP method it returns with different results:

- GET
    Returns the complete string itself
- POST
    Returns a string and concatenates it with `" created"`
    For example to `"Hello World"` string it should return `"Hello World created"`
- DELETE
    Returns half of the string (length/2 rounded down ==> 9 -> 4)
    For example, to `"Hello World"` string should return `"Hello"`
- PUT
    Double the string
    For example to `"Hello World"` string should return `"Hello WorldHello World"`

## Aditional requirements

- Each task is implemented in separate `*.py` files
- Files are added to the `src` folder
- Each task testable via Postman calls
